package http

import (
	"encoding/json"
	"net/http"
)

type SuccessResponse struct {
	StatusCode int         `json:"status_code"`
	Data       interface{} `json:"data"`
}

type ErrorResponse struct {
	StatusCode   int    `json:"code"`
	Name string `json:"message"`
}

func writeSuccessResponse(w http.ResponseWriter, statusCode int, data interface{}) {
	bte, _ := json.Marshal(data)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(bte)
}

func writeErrorResponse(w http.ResponseWriter, statusCode int, err error) {
	var resp ErrorResponse
	if err == nil {
		resp.Name = "FAILED!"
	} else {
		resp.Name = err.Error()
	}
	bte, _ := json.Marshal(resp)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(bte)
}