package http

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/zenport.io/go-assignment/engine"
)

type HTTPAdapter struct{
	srv *http.Server
}

func (adapter *HTTPAdapter) Start() {
	// todo: start to listen
	log.Println("Starting up server...")
	go func() {
		if err := adapter.srv.ListenAndServe(); err != nil {
			log.Fatalf("Failed to start server, err: %s", err.Error())
		}
	}()
}

func (adapter *HTTPAdapter) Stop() {
	// todo: shutdown server
	log.Println("Shutting down server...")
	if err := adapter.srv.Shutdown(context.Background()); err != nil {
		log.Fatalf("Failed to shut down server, err: %s", err.Error())
	}
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
	// todo: init your http server and routes
	app := NewKnightApp(e)
	app.RegisterAllRoutes()

	return &HTTPAdapter{
		srv: &http.Server{
			Addr: ":8080",
			Handler: app.r,
		},
	}
}
