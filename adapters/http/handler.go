package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zenport.io/go-assignment/engine"
	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightApp struct {
	e engine.Engine
	r *mux.Router
}

func NewKnightApp(e engine.Engine) *knightApp {
	return &knightApp{
		e: e,
		r: mux.NewRouter(),
	}
}

func (k *knightApp) RegisterAllRoutes() {
	k.r.HandleFunc("/knight", k.findAll).Methods("GET")
	k.r.HandleFunc("/knight", k.save).Methods("POST")
	k.r.HandleFunc("/knight/{id}", k.find).Methods("GET")

	k.r.NotFoundHandler = http.HandlerFunc(NotFound)
}

func (k knightApp) findAll(w http.ResponseWriter, r *http.Request) {
	knights := k.e.ListKnights()
	writeSuccessResponse(w, http.StatusOK, knights)
}

func (k knightApp) find(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	knight, err := k.e.GetKnight(vars["id"])
	if err != nil {
		writeErrorResponse(w, http.StatusNotFound, err)
		return
	}
	writeSuccessResponse(w, http.StatusOK, knight)
}

func (k knightApp) save(w http.ResponseWriter, r *http.Request) {
	response := map[string]interface{}{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&response)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, err)
		return
	}

	knight := constructRequestParamsToKnightStruct(response)
	if knight == nil {
		writeErrorResponse(w, http.StatusBadRequest, errors.New("knight is empty."))
		return
	}
	if knight.Name == "" {
		writeErrorResponse(w, http.StatusBadRequest, errors.New("ID is empty."))
		return
	}

	err = k.e.SaveKnight(knight)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, err)
		return
	}
	writeSuccessResponse(w, http.StatusCreated, "success")
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	writeErrorResponse(w, http.StatusNotFound, nil)
}

func constructRequestParamsToKnightStruct(m map[string]interface{}) *domain.Knight {
	if m == nil {
		return nil
	}
	knight := &domain.Knight{}
	if _, ok := m["name"]; ok {
		knight.Name = m["name"].(string)
	} else {
		return nil
	}
	if _, ok := m["strength"]; ok {
		knight.Strength = int(m["strength"].(float64))
	} else {
		return nil
	}
	if _, ok := m["weapon_power"]; ok {
		knight.WeaponPower = int(m["weapon_power"].(float64))
	} else {
		return nil
	}

	return knight
}