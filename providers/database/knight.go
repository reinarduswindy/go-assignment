package database

import (
	"database/sql"
	"log"

	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightRepository struct{
	DB *sql.DB
}

const (
	queryGetAllKnights = `SELECT id, name, strength, weapon_power FROM knight`
	queryGetKnightByID = `SELECT id, name, strength, weapon_power FROM knight WHERE id = $1`
	queryInsertKnight  = `INSERT INTO knight(name, strength, weapon_power) VALUES ($1, $2, $3)`
)

func (repository *knightRepository) Find(ID string) (*domain.Knight, error) {
	var knight domain.Knight

	row := repository.DB.QueryRow(queryGetKnightByID, ID)
	err := row.Scan(&knight.ID, &knight.Name, &knight.Strength, &knight.WeaponPower)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
		log.Fatalf("Failed to scan knight, err: %s", err.Error())
	}
	return &knight, nil
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	var knights []*domain.Knight

	rows, err := repository.DB.Query(queryGetAllKnights)
	if err != nil {
		log.Fatalf("Failed to query knights, err: %s", err.Error())
	}

	for rows.Next() {
		var knight domain.Knight
		err = rows.Scan(&knight.ID, &knight.Name, &knight.Strength, &knight.WeaponPower)
		if err != nil {
			log.Fatalf("Failed to scan knight, err: %s", err.Error())
		}
		knights = append(knights, &knight)
	}
	return knights
}

func (repository *knightRepository) Save(knight *domain.Knight) error {
	_, err := repository.DB.Exec(queryInsertKnight,
		knight.Name,
		knight.Strength,
		knight.WeaponPower)
	if err != nil {
		log.Fatalf("Failed to save knight, err: %s", err.Error())
	}
	return nil
}