package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"

	"gitlab.com/zenport.io/go-assignment/engine"
)

const (
	dbUser = "postgres"
	dbPass = "postgres"
	dbName = "test"
)

type Provider struct {
	DB *sql.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{
		DB: provider.DB,
	}
}

func (provider *Provider) Close() {
	provider.DB.Close()
}

func NewProvider() *Provider {
	conn := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", dbUser, dbPass, dbName)
	db, err := sql.Open("postgres", conn)
	if err != nil {
		log.Fatalf("Failed to open db connection, err: %s", err.Error())
	}
	return &Provider{
		DB: db,
	}
}
