package engine

import (
	"errors"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter, err := engine.knightRepository.Find(ID)
	if err != nil || fighter == nil {
		return nil, errors.New(fmt.Sprintf("Knight #%s not found.", ID))
	}
	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) SaveKnight(knight *domain.Knight) error {
	return engine.knightRepository.Save(knight)
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) (domain.Fighter, error) {
	fighter1, err := engine.knightRepository.Find(fighter1ID)
	if err != nil {
		return nil, err
	}
	fighter2, err := engine.knightRepository.Find(fighter2ID)
	if err != nil {
		return nil, err
	}
	return engine.arena.Fight(fighter1, fighter2), nil
}
